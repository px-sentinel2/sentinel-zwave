package net.poundex.sentinel2.server.zwave.modem;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Optional;

@RequiredArgsConstructor
@Getter
public enum NodeCommandClasses implements NodeCommandClass {
	
	PCController(0x02, 0x01),
	MultiLevelSensor(0x21, 0x01),
	PortableSceneController(0x01, 0x06);
	
	private final int genericCommandClassId;
	private final int specificCommandClassId;

	public static Optional<NodeCommandClass> find(int genericDeviceClass, int specificDeviceClass) {
		return Arrays.stream(NodeCommandClasses.values())
				.map(x -> (NodeCommandClass) x)
				.filter(x -> x.getGenericCommandClassId() == genericDeviceClass &&
						x.getSpecificCommandClassId() == specificDeviceClass)
				.findFirst();
	}
}
