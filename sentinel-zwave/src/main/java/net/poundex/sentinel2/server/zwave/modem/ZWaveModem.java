package net.poundex.sentinel2.server.zwave.modem;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.Hardware;

import java.net.URI;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
public class ZWaveModem implements SentinelObject, Hardware {

	private final String id;
	private final String name;
	private final String modemDevice;

	@Override
	public URI getDeviceId() {
		return URI.create(String.format("zwave://%s/self", name));
	}
	
	public URI nodeDeviceId(int nodeId) {
		return getDeviceId().resolve("" + nodeId + "/self");
	}
}
