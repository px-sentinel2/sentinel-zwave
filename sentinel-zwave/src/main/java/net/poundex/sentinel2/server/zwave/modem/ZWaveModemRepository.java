package net.poundex.sentinel2.server.zwave.modem;

import net.poundex.sentinel2.ReadOnlyRepository;
import reactor.core.publisher.Mono;

public interface ZWaveModemRepository extends ReadOnlyRepository<ZWaveModem> {
	Mono<ZWaveModem> create(String name, String modemDevice);
}
