package net.poundex.sentinel2.server.zwave.port;

import java.net.URI;

public class PortPaths {
	public static final URI MULTI_SENSOR = URI.create("msensor/self");
	public static final URI BATTERY_INDICATOR = URI.create("battery/self");
	public static final URI BOOL_SENSOR = URI.create("boolsensor");
	public static final URI SCENE_CONTROLLER = URI.create("buttons/self");
	public static final URI ALARM = URI.create("alarm/self");
}
