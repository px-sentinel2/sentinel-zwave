package net.poundex.sentinel2.server.zwave.modem;

public record ZWaveNode(ZWaveModem modem, int nodeId, NodeCommandClass nodeCommandClass) {
}
