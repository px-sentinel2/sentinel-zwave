package net.poundex.sentinel2.server.zwave.modem;

public interface NodeCommandClass {
	int getGenericCommandClassId();
	int getSpecificCommandClassId();
}
