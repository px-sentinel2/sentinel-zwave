package net.poundex.sentinel2.server.zwave.device;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.server.device.Device;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Data
public class ZWaveDevice implements Device {

	private final URI deviceId;
	private final ZWaveNode node;
	private final Map<URI, DevicePort<?>> ports = new HashMap<>();
	
	@SuppressWarnings("unchecked")
	@Override
	public <VT extends Value<VT>> Optional<DevicePort<VT>> getPort(URI path) {
		return Optional.ofNullable((DevicePort<VT>) ports.get(path));
	}

	void addPort(DevicePort<?> port) {
		ports.put(port.getPortId(), port);
	}
}
