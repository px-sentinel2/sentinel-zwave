package net.poundex.sentinel2.server.zwave.controller;

import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.messaging.Subscribable;
import net.poundex.sentinel2.server.zwave.modem.ZWaveModem;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;

import java.net.URI;

public interface ZWaveModemEvents {
	void nodeUpdated(ZWaveNode node);
	Subscribable<ZWaveNode> nodeUpdated();

	record ConnectionFailedEvent(ZWaveModem zWaveModem, Throwable throwable) { }
	void connectionFailed(ConnectionFailedEvent connectionFailedEvent);
	Subscribable<ConnectionFailedEvent> connectionFailed();

	record ControllerInfoEvent(ZWaveModem zWaveModem, String libraryVersion, int homeId, byte nodeId) { }
	void controllerInfo(ControllerInfoEvent controllerInfoEvent);
	Subscribable<ControllerInfoEvent> controllerInfo();
	
	record ValuePublishedEvent<VT extends Value<VT>>(ZWaveNode node, URI portPath, Value<VT> value) { }
	void valuePublished(ValuePublishedEvent<?> valuePublishedEvent);
	Subscribable<ValuePublishedEvent<?>> valuePublished();
}
