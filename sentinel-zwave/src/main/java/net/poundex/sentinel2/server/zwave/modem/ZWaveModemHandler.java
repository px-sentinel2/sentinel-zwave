package net.poundex.sentinel2.server.zwave.modem;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.zwave.controller.ZWaveModemEvents;
import net.poundex.sentinel2.server.zwave.device.ZWaveDevice;
import net.poundex.sentinel2.server.zwave.device.ZWaveDeviceService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
@Slf4j
public class ZWaveModemHandler {

	private final ZWaveDeviceService deviceService;
	private final ZWaveModemEvents modemEvents;
	
	@PostConstruct
	public ZWaveModemHandler init() {
		modemEvents.nodeUpdated().subscribe(this::onNodeUpdated);
		modemEvents.connectionFailed().subscribe(this::onConnectionFailed);
		modemEvents.controllerInfo().subscribe(this::onControllerInfo);
		return this;
	}

	private void onNodeUpdated(ZWaveNode node) {
		log.info("Z-Wave node updated: {}", node);
		deviceService.updateDevice(ZWaveDevice.builder()
				.deviceId(node.modem().getDeviceId().resolve("" + node.nodeId() + "/self"))
				.node(node)
				.build());
	}

	private void onControllerInfo(ZWaveModemEvents.ControllerInfoEvent controllerInfoEvent) {
		log.info("ZWave controller info: {}", controllerInfoEvent);
	}

	private void onConnectionFailed(ZWaveModemEvents.ConnectionFailedEvent connectionFailedEvent) {
		log.error("Failed to connect to modem " + connectionFailedEvent.zWaveModem(),
				connectionFailedEvent.throwable());
	}
}
