package net.poundex.sentinel2.server.zwave.controller;

import net.poundex.sentinel2.server.zwave.modem.ZWaveModem;

public interface ModemControllerFactory {
	ModemController createModemController(ZWaveModem modem);
}
