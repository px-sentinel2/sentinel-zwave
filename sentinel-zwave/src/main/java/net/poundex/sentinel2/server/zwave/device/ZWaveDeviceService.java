package net.poundex.sentinel2.server.zwave.device;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.device.Driver;
import net.poundex.sentinel2.server.device.DriverEvents;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.PortEvents;
import net.poundex.sentinel2.server.env.value.ButtonValue;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.zwave.controller.ModemControllerFactory;
import net.poundex.sentinel2.server.zwave.controller.ZWaveModemEvents;
import net.poundex.sentinel2.server.zwave.modem.ZWaveModem;
import net.poundex.sentinel2.server.zwave.modem.ZWaveModemRepository;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Service
@RequiredArgsConstructor
@Slf4j
public class ZWaveDeviceService implements Driver {

	private final DriverEvents driverEvents;
	private final ZWaveModemRepository zWaveModemRepository;
	private final Environment springEnvironment;
	private final ModemControllerFactory modemControllerFactory;
	private final ZWaveModemEvents modemEvents;
	private final PortEvents portEvents;
	
	private final Map<ZWaveNode, ZWaveDevice> devices = new HashMap<>();
	private final Map<DevicePort<?>, Value<?>> lastPublishedValues = new ConcurrentHashMap<>();
	
	@Override
	public String getName() {
		return "zwave";
	}

	@Override
	public void start() {
		modemEvents.valuePublished().subscribe(this::handleValue);
		zWaveModemRepository.findAll().subscribe(this::startModem);
	}

	private void startModem(ZWaveModem modem) {
		log.info("Configuring ZWave Modem {}@{}", modem.getName(), modem.getModemDevice());

		ZWaveModemDevice modemDevice = ZWaveModemDevice.builder()
				.deviceId(modem.getDeviceId())
				.build();
		driverEvents.deviceAdded(modemDevice);
		
		modemControllerFactory.createModemController(modem).start();
	}

	public void updateDevice(ZWaveDevice device) {
		if(devices.containsKey(device.getNode()))
			return;
		
		devices.put(device.getNode(), device);
		driverEvents.deviceAdded(device);
	}

	private <VT extends Value<VT>> void handleValue(ZWaveModemEvents.ValuePublishedEvent<VT> event) {
		log.debug("Z-Wave value published: {}", event);
		ZWaveDevice device = devices.get(event.node());
		DevicePort<VT> devicePort = device.<VT>getPort(event.portPath()).orElseGet(() -> {
			DevicePort<VT> port = DevicePort.simple(event
					.node()
					.modem()
					.nodeDeviceId(event.node().nodeId())
					.resolve(event.portPath()));

			device.addPort(port);
			return port;
		});
		
		if( ! (event.value() instanceof ButtonValue) && Objects.equals(lastPublishedValues.get(devicePort), event.value()))
			return;
		
		log.debug("Z-Wave device value changed, publishing port value at {}", devicePort);
		lastPublishedValues.put(devicePort, event.value());
		portEvents.portValuePublished(new PortEvents.PortValuePublishedEvent<>(devicePort, event.value()));
	}
}
