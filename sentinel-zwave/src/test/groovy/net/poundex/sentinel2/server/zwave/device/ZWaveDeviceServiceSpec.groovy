package net.poundex.sentinel2.server.zwave.device

import net.poundex.sentinel2.server.device.DriverEvents
import net.poundex.sentinel2.server.device.port.PortEvents
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.messaging.Subscribable
import net.poundex.sentinel2.server.zwave.ZWaveTestData
import net.poundex.sentinel2.server.zwave.controller.ModemController
import net.poundex.sentinel2.server.zwave.controller.ModemControllerFactory
import net.poundex.sentinel2.server.zwave.controller.ZWaveModemEvents
import net.poundex.sentinel2.server.zwave.modem.ZWaveModemRepository
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode
import org.springframework.core.env.Environment
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Consumer

class ZWaveDeviceServiceSpec extends Specification implements ZWaveTestData {
	
	DriverEvents driverEvents = Mock()
	ZWaveModemRepository repository = Stub()
	Environment springEnvironment = Stub()
	ModemController modemController = Mock()
	ModemControllerFactory modemControllerFactory = Stub()
	Consumer<ZWaveModemEvents.ValuePublishedEvent> listener
	ZWaveModemEvents modemEvents = Stub() {
		valuePublished() >> ({ listener = it } as Subscribable<ZWaveModemEvents.ValuePublishedEvent>)
	}
	PortEvents portEvents = Mock()

	@Subject
	ZWaveDeviceService deviceService = new ZWaveDeviceService(driverEvents, repository, springEnvironment, modemControllerFactory, modemEvents, portEvents)
	
	void "Register all devices"() {
		given:
		repository.findAll() >> Flux.just(A_MODEM)
		modemControllerFactory.createModemController(A_MODEM) >> modemController
		
		when:
		deviceService.start()

		then:
		1 * driverEvents.deviceAdded(_ as ZWaveModemDevice)
		1 * modemController.start()
	}
	
	void "Announces new ZWave devices as they are discovered"() {
		given:
		ZWaveDevice device = Stub() {
			getNode() >> new ZWaveNode(null, 1, null)
		}
		
		when:
		deviceService.updateDevice(device)
		
		then:
		1 * driverEvents.deviceAdded(device)
	}
	
	void "Publishes port value events for zwave value published events"() {
		setup:
		ZWaveNode node = new ZWaveNode(A_MODEM, 1, null)
		ZWaveDevice device = Stub() {
			getNode() >> node
		}
		deviceService.start()
		deviceService.updateDevice(device)
		Value value = Stub()
		
		when:
		listener.accept(new ZWaveModemEvents.ValuePublishedEvent(node, URI.create("somePort"), value))
		
		then:
		1 * portEvents.portValuePublished({ PortEvents.PortValuePublishedEvent e ->
			e.value() == value &&
				e.port().with {
					portId == URI.create("zwave://zwave0/1/somePort")
				}
		})
	}
}
