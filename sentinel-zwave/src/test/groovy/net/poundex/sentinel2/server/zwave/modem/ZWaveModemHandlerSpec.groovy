package net.poundex.sentinel2.server.zwave.modem

import net.poundex.sentinel2.server.messaging.Subscribable
import net.poundex.sentinel2.server.zwave.ZWaveTestData
import net.poundex.sentinel2.server.zwave.controller.ZWaveModemEvents
import net.poundex.sentinel2.server.zwave.device.ZWaveDevice
import net.poundex.sentinel2.server.zwave.device.ZWaveDeviceService
import spock.lang.Specification
import spock.lang.Subject

import java.util.function.Consumer

class ZWaveModemHandlerSpec extends Specification implements ZWaveTestData {
	
	ZWaveDeviceService deviceService = Mock()
	NodeCommandClass nodeCommandClass = Stub()
	ZWaveNode zWaveNode = new ZWaveNode(A_MODEM, 1, nodeCommandClass)
	Consumer<ZWaveNode> nodeDiscoveredListener
	Consumer<ZWaveModemEvents.ControllerInfoEvent> controllerInfoListener
	ZWaveModemEvents modemEvents = Stub() {
		nodeUpdated() >> (({ nodeDiscoveredListener = it }) as Subscribable<ZWaveNode>)
		controllerInfo() >> (({ controllerInfoListener = it}) as Subscribable<ZWaveModemEvents.ControllerInfoEvent>)
	}
	
	@Subject
	ZWaveModemHandler modemHandler = new ZWaveModemHandler(deviceService, modemEvents).init()
	
	void "Creates device and registers with driver"() {
		given:
		ZWaveDevice expectedDevice = ZWaveDevice.builder()
				.deviceId(A_MODEM.getDeviceId().resolve("${zWaveNode.nodeId()}/self"))
				.node(zWaveNode)
				.build()

		when:
		nodeDiscoveredListener.accept(zWaveNode)

		then:
		1 * deviceService.updateDevice(expectedDevice)
	}
}
