package net.poundex.sentinel2.server.zwave.modem

import spock.lang.Specification

class NodeCommandClassesSpec extends Specification {
	void "Looks up correct NodeCommandClass"(NodeCommandClass commandClass) {
		expect:
		NodeCommandClasses
				.find(commandClass.genericCommandClassId, commandClass.specificCommandClassId)
				.orElseThrow() == commandClass
		
		where:
		commandClass << [
				NodeCommandClasses.PCController, 
				NodeCommandClasses.MultiLevelSensor,
				NodeCommandClasses.PortableSceneController ]
	}
}
