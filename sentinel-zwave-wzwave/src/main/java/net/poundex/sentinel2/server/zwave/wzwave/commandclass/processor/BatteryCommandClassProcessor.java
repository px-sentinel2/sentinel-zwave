package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.BatteryCommandClass;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.env.value.NumericValue;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Stream;

@Component
class BatteryCommandClassProcessor implements CommandClassProcessor<BatteryCommandClass> {
	public static final URI BATTERY = URI.create("battery/self");
	public static final URI BATTERY_LEVEL = BATTERY.resolve("level");
	public static final URI BATTERY_LOW = BATTERY.resolve("low");

	@Override
	public Collection<? extends CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, BatteryCommandClass commandClass) {
		return Stream.of(
						Optional.ofNullable(commandClass.isLowWarning())
								.filter(lw -> lw)
								.map(ignored -> new CommandClassService.PortPathAndValue<>(
										BATTERY_LOW, BooleanValue.TRUE)),
						
						Optional.ofNullable(commandClass.getLevel())
								.map(level -> new CommandClassService.PortPathAndValue<>(
										BATTERY_LEVEL, new NumericValue(level))))

				.filter(Optional::isPresent)
				.map(Optional::get)
				.toList();
	}

}
