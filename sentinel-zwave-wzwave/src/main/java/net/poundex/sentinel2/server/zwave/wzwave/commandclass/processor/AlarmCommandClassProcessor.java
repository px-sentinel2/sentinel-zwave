package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.AlarmCommandClass;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.port.PortPaths;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
class AlarmCommandClassProcessor implements CommandClassProcessor<AlarmCommandClass> {
	@Override
	public Collection<CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, AlarmCommandClass commandClass) {
		return switch(commandClass.getActualType()) {
			case 0x07 -> switch (commandClass.getStatus()) {
				case 0x00 -> Collections.singletonList(
						new CommandClassService.PortPathAndValue<>(PortPaths.ALARM.resolve(DevicePort.MOTION), BooleanValue.FALSE));
				case 0x08 -> Collections.singletonList(
						new CommandClassService.PortPathAndValue<>(PortPaths.ALARM.resolve(DevicePort.MOTION), BooleanValue.TRUE));
				default -> Collections.emptyList();
			};
			default -> Collections.emptyList();
		};
	}
}
