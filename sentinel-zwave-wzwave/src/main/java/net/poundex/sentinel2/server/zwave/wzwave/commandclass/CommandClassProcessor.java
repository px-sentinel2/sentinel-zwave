package net.poundex.sentinel2.server.zwave.wzwave.commandclass;

import com.whizzosoftware.wzwave.commandclass.CommandClass;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;

import java.util.Collection;

public interface CommandClassProcessor<CC extends CommandClass> {
	Collection<? extends CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, CC commandClass);
}
