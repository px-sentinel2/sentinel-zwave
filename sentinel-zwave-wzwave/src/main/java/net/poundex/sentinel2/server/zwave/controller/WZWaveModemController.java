package net.poundex.sentinel2.server.zwave.controller;

import com.whizzosoftware.wzwave.controller.ZWaveControllerListener;
import com.whizzosoftware.wzwave.controller.netty.NettyZWaveController;
import com.whizzosoftware.wzwave.node.NodeInfo;
import com.whizzosoftware.wzwave.node.ZWaveEndpoint;
import io.vavr.control.Try;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.zwave.modem.NodeCommandClasses;
import net.poundex.sentinel2.server.zwave.modem.ZWaveModem;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;

import java.nio.file.Files;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Slf4j
class WZWaveModemController implements ModemController, ZWaveControllerListener {

	@Getter(AccessLevel.PACKAGE)
	private final ZWaveModem modem;
	private final ZWaveModemEvents modemEvents;
	private final CommandClassService commandClassService;
	
	private NettyZWaveController wzController;
	
	@Override
	public void start() {
		wzController = Try.of(() -> Files.createTempDirectory("swzw"))
				.map(tmp -> new NettyZWaveController(modem.getModemDevice(), tmp.toFile()))
				.andThen(wzc -> wzc.setListener(this))
				.andThen(NettyZWaveController::start)
				.get();
	}

	@Override
	public void onZWaveNodeAdded(ZWaveEndpoint zWaveEndpoint) {
		ZWaveNode node = new ZWaveNode(
				modem, zWaveEndpoint.getNodeId(), NodeCommandClasses.find(
						zWaveEndpoint.getGenericDeviceClass(),
						zWaveEndpoint.getSpecificDeviceClass())
				.orElseThrow(() -> new UnknownZWaveDeviceException(zWaveEndpoint)));
		
		modemEvents.nodeUpdated(node);
		
		zWaveEndpoint.getCommandClasses()
				.stream()
				.flatMap(cc -> Try.of(() ->
								commandClassService.processCommandClass(node, cc).stream())
						.onFailure(x -> log.warn("Error processing command class", x))
						.getOrElse(Stream::empty))
				.forEach(pv -> modemEvents.valuePublished(new ZWaveModemEvents.ValuePublishedEvent<>(
						node, pv.portPath(), pv.value())));
	}

	@Override
	public void onZWaveNodeUpdated(ZWaveEndpoint zWaveEndpoint) {
		onZWaveNodeAdded(zWaveEndpoint);
	}

	@Override
	public void onZWaveConnectionFailure(Throwable throwable) {
		modemEvents.connectionFailed(new ZWaveModemEvents.ConnectionFailedEvent(modem, throwable));
	}

	@Override
	public void onZWaveControllerInfo(String libraryVersion, Integer homeId, Byte nodeId) {
		modemEvents.controllerInfo(new ZWaveModemEvents.ControllerInfoEvent(
				modem, libraryVersion, homeId, nodeId));
	}

	@Override
	public void onZWaveInclusionStarted() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onZWaveInclusion(NodeInfo nodeInfo, boolean success) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onZWaveInclusionStopped() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onZWaveExclusionStarted() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onZWaveExclusion(NodeInfo nodeInfo, boolean success) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void onZWaveExclusionStopped() {
		throw new UnsupportedOperationException();
	}
}
