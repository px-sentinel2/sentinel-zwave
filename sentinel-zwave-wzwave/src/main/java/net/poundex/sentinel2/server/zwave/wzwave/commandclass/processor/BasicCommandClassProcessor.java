package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.BasicCommandClass;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
class BasicCommandClassProcessor implements CommandClassProcessor<BasicCommandClass> {

	@Override
	public Collection<CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, BasicCommandClass commandClass) {
		return Collections.emptyList();
	}
}
