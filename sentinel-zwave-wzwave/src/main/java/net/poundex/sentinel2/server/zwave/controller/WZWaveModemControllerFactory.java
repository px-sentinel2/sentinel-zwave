package net.poundex.sentinel2.server.zwave.controller;

import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.server.zwave.modem.ZWaveModem;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class WZWaveModemControllerFactory implements ModemControllerFactory {
	
	private final ZWaveModemEvents modemEvents;
	private final CommandClassService commandClassService;

	@Override
	public ModemController createModemController(ZWaveModem modem) {
		return new WZWaveModemController(modem, modemEvents, commandClassService);
	}
}
