package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.WakeUpCommandClass;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
class WakeUpCommandClassProcessor implements CommandClassProcessor<WakeUpCommandClass> {

	@Override
	public Collection<CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, WakeUpCommandClass commandClass) {
		return Collections.emptyList();
	}
}
