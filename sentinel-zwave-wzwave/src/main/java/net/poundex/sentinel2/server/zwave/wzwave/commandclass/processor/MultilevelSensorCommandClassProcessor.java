package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.MultilevelSensorCommandClass;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.env.value.NumericValue;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

import static net.poundex.sentinel2.server.zwave.port.PortPaths.MULTI_SENSOR;

@Component
class MultilevelSensorCommandClassProcessor implements CommandClassProcessor<MultilevelSensorCommandClass> {

	@Override
	public Collection<CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, MultilevelSensorCommandClass commandClass) {
		if(commandClass.getType() == null)
			return Collections.emptyList();
		
		return switch(commandClass.getType()) {
			case AirTemperature -> 
					Collections.singletonList(new CommandClassService.PortPathAndValue<>(
							MULTI_SENSOR.resolve(DevicePort.TEMPERATURE), 
							new NumericValue(commandClass.getValues().get(0))));
			case Humidity ->
					Collections.singletonList(new CommandClassService.PortPathAndValue<>(
							MULTI_SENSOR.resolve(DevicePort.HUMIDITY),
							new NumericValue(commandClass.getValues().get(0))));
			case Luminance ->
					Collections.singletonList(new CommandClassService.PortPathAndValue<>(
							MULTI_SENSOR.resolve(DevicePort.LUMINANCE),
							new NumericValue(commandClass.getValues().get(0))));
			default -> Collections.emptyList();
		};
	}
}
