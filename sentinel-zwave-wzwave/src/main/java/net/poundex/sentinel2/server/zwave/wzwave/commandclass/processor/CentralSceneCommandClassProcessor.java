package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.CentralSceneCommandClass;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.server.env.value.ButtonValue;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.port.PortPaths;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
class CentralSceneCommandClassProcessor implements CommandClassProcessor<CentralSceneCommandClass> {
	
	private final Map<ZWaveNode, Integer> lastSeenSequenceNumber = new HashMap<>();

	@Override
	public Collection<CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, CentralSceneCommandClass commandClass) {
		if(commandClass.getSequenceNumber() == null)
			return Collections.emptyList();
		
		makeSureSequenceNumberIsSane(node, commandClass.getSequenceNumber());
		
		if(commandClass.getSequenceNumber() <= lastSeenSequenceNumber.getOrDefault(node, (Integer.MIN_VALUE + 1)))
			return Collections.emptyList();
		
		lastSeenSequenceNumber.put(node, commandClass.getSequenceNumber());
		
		return Collections.singletonList(new CommandClassService.PortPathAndValue<>(
				PortPaths.SCENE_CONTROLLER.resolve(commandClass.getSceneNumber().toString()), 
				switch(commandClass.getSceneCommand()) {
					case PUSHED -> new ButtonValue(commandClass.getPushCount(), ButtonValue.Type.PUSHED);
					case BEING_HELD -> new ButtonValue(0, ButtonValue.Type.BEING_HELD);
					case RELEASED_AFTER_HOLD -> new ButtonValue(0, ButtonValue.Type.RELEASED_AFTER_HOLD);
				}));
	}

	private void makeSureSequenceNumberIsSane(ZWaveNode node, Integer sequenceNumber) {
		if ( ! lastSeenSequenceNumber.containsKey(node))
			return;

		Integer lastSeen = lastSeenSequenceNumber.get(node);
		if (Math.abs(Math.abs(lastSeen) - Math.abs(sequenceNumber)) <= 2)
			return;

		log.warn("Sequence number {} is suspicious (last seen is {}) - resetting", sequenceNumber, lastSeen);
		lastSeenSequenceNumber.remove(node);
	}
}
