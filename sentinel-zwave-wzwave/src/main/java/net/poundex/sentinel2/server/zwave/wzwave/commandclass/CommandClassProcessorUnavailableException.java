package net.poundex.sentinel2.server.zwave.wzwave.commandclass;

public class CommandClassProcessorUnavailableException extends RuntimeException {
	public CommandClassProcessorUnavailableException(Class<?> klass) {
		super("Could not find processor for " + klass.getName());
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
