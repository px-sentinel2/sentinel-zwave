package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.NoOperationCommandClass;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
class NoOpCommandClassProcessor implements CommandClassProcessor<NoOperationCommandClass> {

	@Override
	public Collection<CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, NoOperationCommandClass commandClass) {
		return Collections.emptyList();
	}
}
