package net.poundex.sentinel2.server.zwave.wzwave.commandclass;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.whizzosoftware.wzwave.commandclass.CommandClass;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;
import java.net.URI;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Service
public class CommandClassService {
	
	private final Supplier<Stream<CommandClassProcessor<?>>> processorsSupplier;

	private final LoadingCache<Class<?>, CommandClassProcessor<?>> processorCache = Caffeine.newBuilder()
			.build(this::getProcessorFor);
	
	public CommandClassService(ObjectProvider<CommandClassProcessor<?>> processorsProvider) {
		this.processorsSupplier = processorsProvider::stream;
	}

	public record PortPathAndValue<VT extends Value<VT>>(URI portPath, Value<VT> value) { }

	@SuppressWarnings("unchecked")
	public <CC extends CommandClass> Collection<? extends PortPathAndValue<?>> processCommandClass(ZWaveNode node, CC commandClass) {
		return ((CommandClassProcessor<CC>)processorCache.get(commandClass.getClass()))
				.process(node, commandClass);
	}

	@SuppressWarnings("unchecked")
	private <CC extends CommandClass> CommandClassProcessor<CC> getProcessorFor(Class<?> klass) {
		return (CommandClassProcessor<CC>) processorsSupplier.get()
				.filter(accepts(klass))
				.findFirst()
				.orElseThrow(() -> new CommandClassProcessorUnavailableException(klass));
	}

	@SuppressWarnings("unchecked")
	private Predicate<? super CommandClassProcessor<?>> accepts(Class<?> klass) {
		return processor -> GenericTypeResolver.getTypeVariableMap(processor.getClass())
				.entrySet()
				.stream()
				.filter(entry -> entry.getKey().getGenericDeclaration().equals(CommandClassProcessor.class))
				.map(t -> {
					if(t.getValue() instanceof ParameterizedType pt)
						return pt.getRawType();
					return t.getValue();
				})
				.map(t -> (Class<? extends CommandClassProcessor<?>>) t)
				.anyMatch(t -> t.isAssignableFrom(klass));
	}
}
