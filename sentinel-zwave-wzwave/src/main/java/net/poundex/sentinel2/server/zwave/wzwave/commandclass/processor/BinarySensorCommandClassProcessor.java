package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.BinarySensorCommandClass;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static net.poundex.sentinel2.server.zwave.port.PortPaths.BOOL_SENSOR;

@Component
class BinarySensorCommandClassProcessor implements CommandClassProcessor<BinarySensorCommandClass> {

	@Override
	public Collection<CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, BinarySensorCommandClass commandClass) {
		return Collections.singletonList(new CommandClassService.PortPathAndValue<>(BOOL_SENSOR,
				Optional.ofNullable(commandClass.isIdle()).orElse(true)
						? BooleanValue.FALSE
						: BooleanValue.TRUE));
	}
}
