package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor;

import com.whizzosoftware.wzwave.commandclass.ManufacturerSpecificCommandClass;
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
class ManufacturerSpecificCommandClassProcessor implements CommandClassProcessor<ManufacturerSpecificCommandClass> {

	@Override
	public Collection<CommandClassService.PortPathAndValue<?>> process(ZWaveNode node, ManufacturerSpecificCommandClass commandClass) {
		return Collections.emptyList();
	}
}
