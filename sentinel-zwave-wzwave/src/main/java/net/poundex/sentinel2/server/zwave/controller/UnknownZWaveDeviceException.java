package net.poundex.sentinel2.server.zwave.controller;

import com.whizzosoftware.wzwave.node.ZWaveEndpoint;

public class UnknownZWaveDeviceException extends RuntimeException {
	public UnknownZWaveDeviceException(ZWaveEndpoint zWaveEndpoint) {
		super(String.format("Unknown Z-Wave node device: %#x(%#x)",
						zWaveEndpoint.getGenericDeviceClass(),
						zWaveEndpoint.getSpecificDeviceClass()));
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
