package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor

import com.whizzosoftware.wzwave.commandclass.MultilevelSensorCommandClass
import net.poundex.sentinel2.server.device.port.DevicePort
import net.poundex.sentinel2.server.env.value.NumericValue
import net.poundex.sentinel2.server.zwave.port.PortPaths
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService
import spock.lang.Specification
import spock.lang.Subject

class MultilevelSensorCommandClassProcessorSpec extends Specification {
	
	@Subject
	MultilevelSensorCommandClassProcessor processor = new MultilevelSensorCommandClassProcessor()
	
	void "Emits correct values for supported sensor types"(MultilevelSensorCommandClass.Type type, double value, Collection<CommandClassService.PortPathAndValue<?>> expected) {
		given:
		MultilevelSensorCommandClass cc = new MultilevelSensorCommandClass()
		cc.type = type
		cc.values = [value]
		
		expect:
		processor.process(null, cc) == expected

		where:
		type                                             | value || expected
		MultilevelSensorCommandClass.Type.AirTemperature | 22    || [ new CommandClassService.PortPathAndValue<>(PortPaths.MULTI_SENSOR.resolve(DevicePort.TEMPERATURE), new NumericValue(22)) ]
		MultilevelSensorCommandClass.Type.Humidity       | 48    || [ new CommandClassService.PortPathAndValue<>(PortPaths.MULTI_SENSOR.resolve(DevicePort.HUMIDITY), new NumericValue(48)) ]
		MultilevelSensorCommandClass.Type.Luminance      | 96    || [ new CommandClassService.PortPathAndValue<>(PortPaths.MULTI_SENSOR.resolve(DevicePort.LUMINANCE), new NumericValue(96)) ]
	}
}
