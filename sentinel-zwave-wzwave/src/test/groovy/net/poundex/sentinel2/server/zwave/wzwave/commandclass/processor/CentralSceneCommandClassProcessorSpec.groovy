package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor


import com.whizzosoftware.wzwave.commandclass.CentralSceneCommandClass
import net.poundex.sentinel2.server.env.value.ButtonValue
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService
import spock.lang.Specification
import spock.lang.Subject

class CentralSceneCommandClassProcessorSpec extends Specification {
	
	private static final URI PORT_PATH = "buttons/1".toURI()
	private static final ZWaveNode A_NODE = new ZWaveNode(null, 2, null)
	
	@Subject
	CentralSceneCommandClassProcessor processor = new CentralSceneCommandClassProcessor()
	
	void "Emits correct value for scene controller action (if sequence number has increased)"(
			CentralSceneCommandClass cc, Collection<CommandClassService.PortPathAndValue<ButtonValue>> expected) {
		expect:
		processor.process(A_NODE, cc) == expected

		where:
		cc                                                             || expected
		commandClass(1, ButtonValue.Type.PUSHED, 1, 1)                 || [portPathAndValue(new ButtonValue(1, ButtonValue.Type.PUSHED))]
		commandClass(2, ButtonValue.Type.PUSHED, 1, 2)                 || [portPathAndValue(new ButtonValue(2, ButtonValue.Type.PUSHED))]
		commandClass(2, ButtonValue.Type.PUSHED, 1, Integer.MIN_VALUE) || []
		commandClass(0, ButtonValue.Type.BEING_HELD, 1, 3)             || [portPathAndValue(new ButtonValue(0, ButtonValue.Type.BEING_HELD))]
		commandClass(0, ButtonValue.Type.RELEASED_AFTER_HOLD, 1, 4)    || [portPathAndValue(new ButtonValue(0, ButtonValue.Type.RELEASED_AFTER_HOLD))]
	}

	CommandClassService.PortPathAndValue portPathAndValue(ButtonValue value) {
		return new CommandClassService.PortPathAndValue<>(PORT_PATH, value)
	}
	
	CentralSceneCommandClass commandClass(int pc, ButtonValue.Type t, int scnNo, int seqNo) {
		return new CentralSceneCommandClass().tap {
			pushCount = pc 
			sceneCommand = switch(t) {
				case ButtonValue.Type.PUSHED -> CentralSceneCommandClass.SceneCommand.PUSHED
				case ButtonValue.Type.BEING_HELD -> CentralSceneCommandClass.SceneCommand.BEING_HELD
				case ButtonValue.Type.RELEASED_AFTER_HOLD -> CentralSceneCommandClass.SceneCommand.RELEASED_AFTER_HOLD
			}
			sequenceNumber = seqNo
			sceneNumber = scnNo
		}
	}
}
