package net.poundex.sentinel2.server.zwave.controller

import com.whizzosoftware.wzwave.commandclass.CommandClass
import com.whizzosoftware.wzwave.node.ZWaveEndpoint
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.zwave.ZWaveTestData
import net.poundex.sentinel2.server.zwave.modem.NodeCommandClasses
import net.poundex.sentinel2.server.zwave.modem.ZWaveNode
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService
import spock.lang.Specification
import spock.lang.Subject

class WZWaveModemControllerSpec extends Specification implements ZWaveTestData {
	
	private static final URI PORT_PATH_1 = "ports/1".toURI()
	private static final URI PORT_PATH_2 = "ports/2".toURI()
	private static final URI PORT_PATH_3 = "ports/3".toURI()
	Value value1 = Stub()
	Value value2 = Stub()
	Value value3 = Stub()
	ZWaveModemEvents modemEvents = Mock()
	CommandClass commandClass1 = Stub()
	CommandClass commandClass2 = Stub()
	ZWaveEndpoint wzEndpoint = Stub() {
		getNodeId() >> 1
		getGenericDeviceClass() >> NodeCommandClasses.MultiLevelSensor.genericCommandClassId
		getSpecificDeviceClass() >> NodeCommandClasses.MultiLevelSensor.specificCommandClassId
		getCommandClasses() >> [commandClass1, commandClass2]
	}
	CommandClassService commandClassService = Stub() {
		processCommandClass(_, commandClass1) >>
				[ new CommandClassService.PortPathAndValue<Value>(PORT_PATH_1, value1) ]
		processCommandClass(_, commandClass2) >> 
				[ new CommandClassService.PortPathAndValue<Value>(PORT_PATH_2, value2), 
				  new CommandClassService.PortPathAndValue<Value>(PORT_PATH_3, value3) ]
	}
	
	@Subject
	WZWaveModemController modemController = new WZWaveModemController(A_MODEM, modemEvents, commandClassService)
	
	void "Handles node update"() {
		given:
		ZWaveNode expectedNode = new ZWaveNode(A_MODEM, 1, NodeCommandClasses.MultiLevelSensor)
		ZWaveModemEvents.ValuePublishedEvent event1 = 
				new ZWaveModemEvents.ValuePublishedEvent(expectedNode, PORT_PATH_1, value1)
		ZWaveModemEvents.ValuePublishedEvent event2 = 
				new ZWaveModemEvents.ValuePublishedEvent(expectedNode, PORT_PATH_2, value2)
		ZWaveModemEvents.ValuePublishedEvent event3 =
				new ZWaveModemEvents.ValuePublishedEvent(expectedNode, PORT_PATH_3, value3)
		
		when:
		modemController.onZWaveNodeAdded(wzEndpoint)
		
		then:
		1 * modemEvents.nodeUpdated(expectedNode)
		1 * modemEvents.valuePublished(event1)
		1 * modemEvents.valuePublished(event2)
		1 * modemEvents.valuePublished(event3)
	}
	
	void "Handles connection failure"() {
		given:
		Throwable ex = Stub()
		
		when:
		modemController.onZWaveConnectionFailure(ex)
		
		then:
		1 * modemEvents.connectionFailed(
				new ZWaveModemEvents.ConnectionFailedEvent(A_MODEM, ex))
	}
	
	void "Handles controller info"() {
		when:
		modemController.onZWaveControllerInfo("1", 2, 3 as Byte)
		
		then:
		1 * modemEvents.controllerInfo(new ZWaveModemEvents.ControllerInfoEvent(
				A_MODEM, "1", 2, 3 as byte))
	}
}
