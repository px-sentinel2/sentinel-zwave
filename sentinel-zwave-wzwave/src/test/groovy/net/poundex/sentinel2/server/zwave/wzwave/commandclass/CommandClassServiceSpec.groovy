package net.poundex.sentinel2.server.zwave.wzwave.commandclass

import com.whizzosoftware.wzwave.commandclass.BinarySensorCommandClass
import com.whizzosoftware.wzwave.commandclass.MultilevelSensorCommandClass
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.test.ProcessorA
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.test.ProcessorB
import org.springframework.beans.factory.ObjectProvider
import spock.lang.Specification
import spock.lang.Subject

class CommandClassServiceSpec extends Specification {
	
	Value value1 = Stub()
	Value value2 = Stub()
	Value value3 = Stub()
	
	CommandClassService.PortPathAndValue pv1 = new CommandClassService.PortPathAndValue("port/1".toURI(), value1)
	CommandClassService.PortPathAndValue pv2 = new CommandClassService.PortPathAndValue("port/2".toURI(), value2)
	CommandClassService.PortPathAndValue pv3 = new CommandClassService.PortPathAndValue("port/3".toURI(), value3)
	
	MultilevelSensorCommandClass cc1 = new MultilevelSensorCommandClass()
	BinarySensorCommandClass cc2 = new BinarySensorCommandClass()

	ProcessorA processorA = Stub(ProcessorA) {
		process(null, cc1) >> [pv1, pv2]
	}

	ProcessorB procesorB = Stub(ProcessorB) {
		process(null, cc2) >> [pv3]
	}

	ObjectProvider<CommandClassProcessor<?>> processors = Stub() {
		stream() >> { [processorA, procesorB].stream() }
	}
	
	@Subject
	CommandClassService service = new CommandClassService(processors)
	
	void "Looks up correct processor and returns emitted values"() {
		expect:
		service.processCommandClass(null, cc1) == [pv1, pv2]
		service.processCommandClass(null, cc2) == [pv3]
	}
}
