package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor

import com.whizzosoftware.wzwave.commandclass.BatteryCommandClass
import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.env.value.NumericValue
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService
import spock.lang.Specification
import spock.lang.Subject

class BatteryCommandClassProcessorSpec extends Specification {
	
	private static final BatteryCommandClass BATTERY_LEVEL_CC = new BatteryCommandClass().tap {
		level = 0x32
	}

	private static final BatteryCommandClass BATTERY_LOW_CC = new BatteryCommandClass().tap {
		lowWarning = true
	}
	
	@Subject
	BatteryCommandClassProcessor processor = new BatteryCommandClassProcessor()
	
	void "Emits correct value for battery report"(
			BatteryCommandClass cc, Collection<CommandClassService.PortPathAndValue<?>> expected) {

		expect:
		processor.process(null, cc) == expected

		where:
		cc               || expected
		BATTERY_LEVEL_CC || [ portPathAndValue("battery/level", new NumericValue(50)) ]
		BATTERY_LOW_CC   || [ portPathAndValue("battery/low", BooleanValue.TRUE) ]
	}

	CommandClassService.PortPathAndValue portPathAndValue(String portPath, Value value) {
		return new CommandClassService.PortPathAndValue<>(portPath.toURI(), value)
	}
}
