package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor

import com.whizzosoftware.wzwave.commandclass.BinarySensorCommandClass
import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.zwave.port.PortPaths
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService
import spock.lang.Specification
import spock.lang.Subject

class BinarySensorCommandClassProcessorSpec extends Specification {
	
	@Subject
	BinarySensorCommandClassProcessor processor = new BinarySensorCommandClassProcessor()
	
	void "Emits correct value for sensor state"(boolean idle, Collection<CommandClassService.PortPathAndValue<?>> expected) {
		given:
		BinarySensorCommandClass cc = new BinarySensorCommandClass()
		cc.isIdle = idle

		expect:
		processor.process(null, cc) == expected

		where:
		idle  || expected
		true  || [ new CommandClassService.PortPathAndValue<>(PortPaths.BOOL_SENSOR, BooleanValue.FALSE) ]
		false || [ new CommandClassService.PortPathAndValue<>(PortPaths.BOOL_SENSOR, BooleanValue.TRUE) ]
	}
}
