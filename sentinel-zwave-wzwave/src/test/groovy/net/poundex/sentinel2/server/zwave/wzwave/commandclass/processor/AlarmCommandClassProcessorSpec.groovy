package net.poundex.sentinel2.server.zwave.wzwave.commandclass.processor

import com.whizzosoftware.wzwave.commandclass.AlarmCommandClass
import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassService
import spock.lang.Specification
import spock.lang.Subject

class AlarmCommandClassProcessorSpec extends Specification {
	
	private static final MOTION_ON_CC = new AlarmCommandClass().tap {
		actualType = 0x07
		status = 0x08
	}

	private static final MOTION_OFF_CC = new AlarmCommandClass().tap {
		actualType = 0x07
		status = 0x00
	}

	private static final TAMPER_CC = new AlarmCommandClass().tap {
		actualType = 0x07
		status = 0x03
	}

	private static final GARBAGE_CC = new AlarmCommandClass().tap {
		actualType = 0x01
		status = 0x02
	}
	
	@Subject
	AlarmCommandClassProcessor processor = new AlarmCommandClassProcessor()
	
	void "Emits correct value for alarm"(
			AlarmCommandClass cc, Collection<CommandClassService.PortPathAndValue<?>> expected) {

		expect:
		processor.process(null, cc) == expected

		where:
		cc            || expected
		MOTION_ON_CC  || [portPathAndValue("alarm/motion", BooleanValue.TRUE)]
		MOTION_OFF_CC || [portPathAndValue("alarm/motion", BooleanValue.FALSE)]
		TAMPER_CC     || []
		GARBAGE_CC    || []
	}

	CommandClassService.PortPathAndValue portPathAndValue(String portPath, Value value) {
		return new CommandClassService.PortPathAndValue<>(portPath.toURI(), value)
	}

}
