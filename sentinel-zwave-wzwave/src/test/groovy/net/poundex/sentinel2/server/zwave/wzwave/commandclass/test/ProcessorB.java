package net.poundex.sentinel2.server.zwave.wzwave.commandclass.test;

import com.whizzosoftware.wzwave.commandclass.BinarySensorCommandClass;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;

public interface ProcessorB extends CommandClassProcessor<BinarySensorCommandClass> {
}
