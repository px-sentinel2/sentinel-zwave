package net.poundex.sentinel2.server.zwave.wzwave.commandclass.test;

import com.whizzosoftware.wzwave.commandclass.MultilevelSensorCommandClass;
import net.poundex.sentinel2.server.zwave.wzwave.commandclass.CommandClassProcessor;

public interface ProcessorA extends CommandClassProcessor<MultilevelSensorCommandClass> {
}
