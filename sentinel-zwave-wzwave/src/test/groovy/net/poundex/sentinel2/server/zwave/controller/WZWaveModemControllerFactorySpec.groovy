package net.poundex.sentinel2.server.zwave.controller

import net.poundex.sentinel2.server.zwave.ZWaveTestData
import spock.lang.Specification
import spock.lang.Subject

class WZWaveModemControllerFactorySpec extends Specification implements ZWaveTestData {
	
	@Subject
	WZWaveModemControllerFactory factory = new WZWaveModemControllerFactory(null, null)
	
	void "Creates new ZWaveModemControllers"() {
		when:
		ModemController controller = factory.createModemController(A_MODEM)
		
		then:
		controller instanceof WZWaveModemController
		with(controller as WZWaveModemController) {
			modem == A_MODEM
		}
	}
}
