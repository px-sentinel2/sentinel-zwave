package net.poundex.sentinel2.server.zwave

import net.poundex.sentinel2.server.zwave.modem.ZWaveModem

// TODO - Duplicated from sentinel-zwave
interface ZWaveTestData {
	static final ZWaveModem A_MODEM = ZWaveModem.builder()
			.name("zwave0")
			.modemDevice("/some/device")
			.build()
}